/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_defines.h                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/12 16:50:41 by lbrangie          #+#    #+#             */
/*   Updated: 2018/03/09 15:26:14 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_DEFINES_H
# define FT_PRINTF_DEFINES_H

# ifndef PRINTF_BUFF_SIZE
#  define PRINTF_BUFF_SIZE 1024
# endif

# ifndef PRINTF_FLAGS
#  define PRINTF_FLAGS "#0-+ "
# endif

# ifndef PRINTF_F_HASH
#  define PRINTF_F_HASH 0b00000001
# endif
# ifndef PRINTF_F_ZERO
#  define PRINTF_F_ZERO 0b00000010
# endif
# ifndef PRINTF_F_MINU
#  define PRINTF_F_MINU 0b00000100
# endif
# ifndef PRINTF_F_PLUS
#  define PRINTF_F_PLUS 0b00001000
# endif
# ifndef PRINTF_F_SPAC
#  define PRINTF_F_SPAC 0b00010000
# endif

#endif
