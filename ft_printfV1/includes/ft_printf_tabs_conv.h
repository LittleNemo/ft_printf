/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_tabs_conv.h                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/12 16:45:56 by lbrangie          #+#    #+#             */
/*   Updated: 2018/03/09 13:14:14 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_TABS_CONV_H
# define FT_PRINTF_TABS_CONV_H

typedef struct	s_conv
{
	char			flag;
	int				(*f)(t_flags *, char *, va_list);
}				t_conv;

struct s_conv	g_tab_conv[4] =
{
	{'d', ptf_put_dec},	
//	{'D', ptf_out_ldec},	//not done yet

	{'i', ptf_put_dec},	

//	{'u', ptf_put_udec},	//not done yet
//	{'U', ptf_put_ludec},	//not done yet

	{'x', ptf_put_hex},		//not done yet
//	{'X', ptf_put_mhex},	//not done yet

//	{'o', ptf_put_oct},		//not done yet
//	{'O', ptf_put_uoct},	//not done yet

//	{'b', ptf_put_bin},		//not done yet
//	{'B', ptf_put_mbin},	//not done yet

	{'c', ptf_put_char},
//	{'C', ptf_put_wchar},	//not done yet

	{'s', ptf_put_str},
//	{'S', ptf_put_wstr},	//not done yet

//	{'p', ptf_put_ptr},		//not done yet

};

#endif
