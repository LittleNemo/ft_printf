/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_tabs_colors.h                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/12 16:47:37 by lbrangie          #+#    #+#             */
/*   Updated: 2018/03/09 15:26:08 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_TABS_COLORS_H
# define FT_PRINTF_TABS_COLORS_H

typedef struct	s_colors
{
	char			*c_flag;
	char			*to_print;
}				t_colors;

struct s_colors	g_tab_colors[27] =
{
	{"{eoc}", "\e[0m"},
	{"{bold}", "\e[1m"},
	{"{dim}", "\e[2m"},
	{"{underline}", "\e[4m"},
	{"{blink}", "\e[5m"},
	{"{red}", "\e[31m"},
	{"{green}", "\e[32m"},
	{"{blue}", "\e[34m"},
	{"{white}", "\e[97m"},
	{"{cyan}", "\e[36m"},
	{"{magenta}", "\e[35m"},
	{"{yellow}", "\e[33m"},
	{"{black}", "\e[30m"},
	{"{light_blue}", "\e[94m"},
	{"{purple}", "\e[38;5;55m"},
	{"{orange}", "\e[38;5;202m"},

	{"{Bred}", "\e[41m"},
	{"{Bgreen}", "\e[42m"},
	{"{Bblue}", "\e[44m"},
	{"{Bwhite}", "\e[107m"},
	{"{Bcyan}", "\e[46m"},
	{"{Bmagenta}", "\e[45m"},
	{"{Byellow}", "\e[43m"},
	{"{Bblack}", "\e[40m"},
	{"{Blight_blue}", "\e[104m"},
	{"{Bpurple}", "\e[48;5;55m"},
	{"{Borange}", "\e[48;5;202m"},
};

#endif
