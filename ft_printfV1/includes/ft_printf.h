/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/08 13:41:14 by lbrangie          #+#    #+#             */
/*   Updated: 2018/03/09 13:26:40 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdarg.h>
# include <stdio.h>
# include "../srcs/libft/includes/libft.h"
# include "ft_printf_defines.h"

/*
**	The attribut flags ('#', '0', '-', '+' and ' ') are stored, using bitwise,
**	in the p_flags char, that can be represented as follow:
**		[][][][ ][+][-][0][#]
**	The length modifiers ('h', 'l', 'j', 'z', 'hh' and 'll') are stored in the
**	string length.
**	The field width is stored in 'field' and the precision in 'pres'.
*/

typedef struct	s_flags
{
	char			p_flags;
	char			len[4];
	int				field;
	int				pres;
}				t_flags;

int				ft_printf(const char *format, ...);
int				ft_fprintf(FILE *stream, const char *format, ...);
int				ft_dprintf(int fd, const char *format, ...);
int				ft_vprintf(const char *format, va_list ap);
int				ft_vfprintf(FILE *stream, const char *format, va_list ap);
int				ft_vdprintf(int fd, const char *format, va_list ap);

const char		*ptf_get_color(const char *format, char **color);
const char		*ptf_get_flags(t_flags *flags, const char *format, va_list ap);
int				ptf_get_field(t_flags *flags, const char *format, va_list ap);
int				ptf_get_pres(t_flags *flags, const char *format, va_list ap);
void			ptf_init_flags(t_flags *flags, char **str_flags);

int				ptf_isconv(char c);
int				ptf_isflag(char c);
int				ptf_ishash(t_flags *flags);

int				ptf_put_char(t_flags *flags, char *buff, va_list ap);
int				ptf_put_dec(t_flags *flags, char *buff, va_list ap);
int				ptf_put_hex(t_flags *flags, char *buff, va_list ap);
int				ptf_put_str(t_flags *flags, char *buff, va_list ap);

int				ptf_select_cast(t_flags *flags, va_list ap);
int				ptf_select_color(const char *format, char *buff);
int				ptf_select_conv(t_flags *flags, const char *format, \
				char *buff, va_list ap);
intmax_t		ptf_select_ucast(t_flags *flags, va_list ap);

#endif
