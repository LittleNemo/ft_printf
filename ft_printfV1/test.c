/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/10 13:06:22 by lbrangie          #+#    #+#             */
/*   Updated: 2018/03/09 14:53:29 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <limits.h>
#include <locale.h>
#include "ft_printf.h"

int				main()
{
/*	t_flags	flags;

	ptf_init_flags(&flags);
//	flags.p_flags |= PRINTF_F_SPAC;
//	flags.p_flags |= PRINTF_F_HASH;
	flags.p_flags |= PRINTF_F_LL;
//	flags.p_flags |= PRINTF_F_PLUS;
//	flags.p_flags |= PRINTF_F_PLUS;
	if (flags.p_flags & PRINTF_F_HASH)
		printf("Flag '#' detected!\n");
	if (flags.p_flags & PRINTF_F_ZERO)
		printf("Flag '0' detected!\n");
	if (flags.p_flags & PRINTF_F_MINU)
		printf("Flag '-' detected!\n");
	if (flags.p_flags & PRINTF_F_PLUS)
		printf("Flag '+' detected!\n");
	if (flags.p_flags & PRINTF_F_SPAC)
		printf("Flag ' ' detected!\n");
	if (flags.p_flags & PRINTF_F_H)
		printf("Flag 'h' detected!\n");
	if (flags.p_flags & PRINTF_F_HH)
		printf("Flag 'hh' detected!\n");
	if (flags.p_flags & PRINTF_F_L)
		printf("Flag 'l' detected!\n");
	if (flags.p_flags & PRINTF_F_LL)
		printf("Flag 'll' detected!\n");
	if (flags.p_flags & PRINTF_F_Z)
		printf("Flag 'z' detected!\n");
	if (flags.p_flags & PRINTF_F_J)
		printf("Flag 'j' detected!\n");
	printf("flags->p_flags : %U\n", flags.p_flags);
	return (0);
	void			*test = "test";
	int				*i;*/

/*	printf("%2$s '%1$#-10x' %2$s\n", 65, "Hello World!");
	printf("Hello World! '%0+10d' Hello World!\n", -42);
	printf("Hello World! '%#010.5x' Hello World!\n", 65);
	printf("Hello World! '%.0d' Hello World!\n", 1);
	printf("test %%p : '%p'\n", test);
	printf("=============== TEST OCTAL/DECIMAL/HEXA==============\n");
	printf("42 dec : '%d'\n", 42);
	printf("42 oct : '%o'\n", 42);
	printf("42 Oct : '%O'\n", 42);
	printf("42 hex : '%x'\n", 42);
	printf("42 Hex : '%X'\n", 42);
	printf("tests : %d\n", INT_MAX + 1);
	printf("test pres -1: '%-15.5d'\n", INT_MAX);*/
//	printf("test pres : '%d'\n", 18);
//	ft_printf("test 1 : i'm here\n");
//	ft_printf("test 2 : '%*.*hd' i'm hidden '% *.*ld'\n", 2, 4, -8, 16);
//	ft_printf("test 2 : '%+#0 lld' i'm hidden '%-z", 1, 1);
	setlocale(LC_ALL, "");
	int test = 0;
	int		field = 10;
	int		pres = 0;
	int		ret_ptf;
	int		ret_ft;

	ret_ptf = printf("printf:    '%#*.*d' i'm hidden", field, pres, test);
//	ret_ptf = printf("printf:    '-x' i'm hidden\n", field, pres, "abcdef");
	printf("\tret: %d\n", ret_ptf);
	ret_ft = ft_printf("ft_printf: '%#*.*d{eoc}' i'm hidden", field, pres, test);
	printf("\tret: %d\n", ret_ft);
//	printf("=====> TOTAL: %d <=====\n", ft_putfile("srcs/libft/includes/libft.h"));
//	ft_printf("ft_printf:\t'%-.6s' i'm hidden\n", test);
/*	printf("hh\tsizeof(signed char): %lu\n", sizeof(signed char));
	printf("h\tsizeof(short): %lu\n", sizeof(short));
	printf("l\tsizeof(long): %lu\n", sizeof(long));
	printf("ll\tsizeof(long long): %lu\n", sizeof(long long));
	printf("j\tsizeof(intmax_t): %lu\n", sizeof(intmax_t));
	printf("\tsizeof(uintmax_t): %lu\n", sizeof(uintmax_t));
	printf("\tsizeof(size_t): %lu\n", sizeof(size_t));
	printf("z\tsizeof(ssize_t): %lu\n", sizeof(ssize_t));
*/
//	printf("test 2 : '%##x' i'm hidden '%yo'\n", 42);
//	printf("test 2 : '%d' i'm hidden '%-z' test123\n", 1);
//	printf("return : '%i'\n", ft_printf(NULL));
//	ft_printf("test : '%%'\n");
//	printf("test largeur champs : '%*.0d'\n", 0, 0);
	return (0);
}
