/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ptf_put_dec.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/12 13:33:14 by lbrangie          #+#    #+#             */
/*   Updated: 2018/03/08 14:54:49 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static char		*ptf_write_dec(char *buff, int d, int nlen)
{
	char			*str_base;
	int				i;

	str_base = STR_BASE;
	i = 0;
	if (d < 0)
		*buff++ = '-';
	while (i++ < nlen - 1)
		buff++;
	while (nlen--)
	{
		*buff-- = str_base[ft_abs(d % 10)];
		d /= 10;
	}
	buff += i;
	return (buff);
}

static char		*ptf_write_fill(char *buff, int flen)
{
	while (flen--)
		*buff++ = ' ';
	return (buff);
}

int				ptf_put_dec(t_flags *flags, char *buff, va_list ap)
{
	int				d;
	int				nlen;
	int				flen;
	int				ret;

	d = ptf_select_cast(flags, ap);
	nlen = ft_numlen_base(d, 10);
	flen = 0;
	if ((flags->pres >= 0) && (flags->pres > nlen))
		nlen = flags->pres;
	if (flags->field > nlen)
		flen = flags->field - nlen;
	ret = flen + nlen + ft_isneg(d);
	if (flags->p_flags & PRINTF_F_MINU)
		buff = ptf_write_dec(buff, d, nlen) + 1;
	buff = ptf_write_fill(buff, flen);
	if (flags->p_flags ^ PRINTF_F_MINU)
		buff = ptf_write_dec(buff, d, nlen);
	return (ret);
}
