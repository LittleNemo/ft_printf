/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ptf_ctype.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/01 15:59:54 by lbrangie          #+#    #+#             */
/*   Updated: 2018/03/09 13:26:04 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int				ptf_isconv(char c)
{
	return (c == 'd' || c == 'D' || c == 'i' || c == 'u' || c == 'U' || \
			c == 'x' || c == 'X' || c == 'o' || c == 'O' || c == 'b' || \
			c == 'B' || c == 'c' || c == 'C' || c == 's' || c == 'S' || \
			c == 'p');
}

int				ptf_isflag(char c)
{
	return (c == '#' || c == '0' || c == '-' || c == '+' || c == ' ' || \
			c == 'h' || c == 'l' || c == 'j' || c == 'z' || c == '.' || \
			c == '*' || ft_isdigit(c));
}

int				ptf_ishash(t_flags *flags)
{
	if (flags->p_flags & PRINTF_F_HASH)
		return (2);
	return (0);
}
