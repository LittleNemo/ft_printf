/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ptf_select.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/01 15:50:14 by lbrangie          #+#    #+#             */
/*   Updated: 2018/03/09 11:52:25 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "ft_printf_tabs_colors.h"
#include "ft_printf_tabs_conv.h"

int				ptf_select_cast(t_flags *flags, va_list ap)
{
	int				d;

	if (ft_strlen(flags->len) == 3)
		d = (long long)va_arg(ap, int);
	else if ((flags->len[0] == 'h') && (!flags->len[1]))
		d = (short)va_arg(ap, int);
	else if ((flags->len[0] == 'h') && (flags->len[1] == 'h'))
		d = (signed char)va_arg(ap, int);
	else if ((flags->len[0] == 'l') && (!flags->len[1]))
		d = (long)va_arg(ap, int);
	else if ((flags->len[0] == 'l') && (flags->len[1] == 'l'))
		d = (long long)va_arg(ap, int);
	else
		d = (long long)va_arg(ap, int);
	return (d);
}

intmax_t		ptf_select_ucast(t_flags *flags, va_list ap)
{
	intmax_t		d;

	if (ft_strlen(flags->len) == 3)
		d = (long long)va_arg(ap, intmax_t);
	else if ((flags->len[0] == 'h') && (!flags->len[1]))
		d = (short)va_arg(ap, intmax_t);
	else if ((flags->len[0] == 'h') && (flags->len[1] == 'h'))
		d = (signed char)va_arg(ap, intmax_t);
	else if ((flags->len[0] == 'l') && (!flags->len[1]))
		d = (long)va_arg(ap, intmax_t);
	else if ((flags->len[0] == 'l') && (flags->len[1] == 'l'))
		d = (long long)va_arg(ap, intmax_t);
	else
		d = (long long)va_arg(ap, intmax_t);
	return (d);
}

int				ptf_select_color(const char *format, char *buff)
{
	unsigned int	i;
	unsigned int	j;
	unsigned int	tlen;

	i = 0;
	tlen = sizeof(g_tab_colors) / sizeof(t_colors);
	while (i < tlen)
	{
		if (!ft_strncmp(format, g_tab_colors[i].c_flag, \
					ft_strlen(g_tab_colors[i].c_flag)))
		{
			j = 0;
			while (g_tab_colors[i].to_print[j])
				*buff++ = g_tab_colors[i].to_print[j++];
			return (ft_strlen(g_tab_colors[i].to_print));
		}
		i++;
	}
	return (0);
}

int				ptf_select_conv(t_flags *flags, const char *format, \
				char *buff, va_list ap)
{
	unsigned int	i;
	unsigned int	tlen;

	i = 0;
	tlen = sizeof(g_tab_conv) / sizeof(t_conv);
	while (i < tlen)
		if (*format == g_tab_conv[i++].flag)
			return (g_tab_conv[--i].f(flags, buff, ap));
	return (0);
}
