/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vprintf.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/11 15:38:27 by lbrangie          #+#    #+#             */
/*   Updated: 2018/03/07 18:15:24 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int				ft_vprintf(const char *format, va_list ap)
{
	return (ft_vfprintf(stdout, format, ap));
}

int				ft_vdprintf(int fd, const char *format, va_list ap)
{
	char			buff[PRINTF_BUFF_SIZE];
	int				i;
	t_flags			flags;

	i = 0;
	while (*format)
	{
		if (*format == '%')
		{
			format = ptf_get_flags(&flags, ++format, ap);
			if (ptf_isconv(*format))
				i += ptf_select_conv(&flags, format++, &buff[i], ap);
		}
		if (*format == '}')
		{
			i += ptf_select_color(format, &buff[i]);
			while (*format != '}')
				format++;
			format++;
		}
		else
			buff[i++] = *format++;
	}
	buff[i] = '\0';
	return (ft_putstr_fd(buff, fd));
}

int				ft_vfprintf(FILE *stream, const char *format, va_list ap)
{
	char			buff[PRINTF_BUFF_SIZE];
	int				i;
	t_flags			flags;

	i = 0;
	while (*format)
	{
		if (*format == '%')
		{
			format = ptf_get_flags(&flags, ++format, ap);
			if (ptf_isconv(*format))
				i += ptf_select_conv(&flags, format++, &buff[i], ap);
		}
		if (*format == '{')
		{
			i += ptf_select_color(format, &buff[i]);
			while (*format != '}')
				format++;
			format++;
		}
		else
			buff[i++] = *format++;
	}
	buff[i] = '\0';
	return (ft_putstr_fd(buff, stream->_file));
}
