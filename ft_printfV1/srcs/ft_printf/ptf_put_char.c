/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ptf_put_char.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/14 16:52:49 by lbrangie          #+#    #+#             */
/*   Updated: 2018/02/19 14:36:04 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int				ptf_put_char(t_flags *flags, char *buff, va_list ap)
{
	char			c;
	char			fill;
	int				len;

	c = va_arg(ap, int);
	fill = ' ';
	len = 1;
	if ((flags->p_flags & PRINTF_F_ZERO) && (flags->p_flags ^ PRINTF_F_MINU))
		fill = '0';
	if (flags->field)
		len = flags->field;
	if (flags->p_flags & PRINTF_F_MINU)
		*buff++ = c;
	while (len--)
		*buff++ = fill;
	if (flags->p_flags ^ PRINTF_F_MINU)
		*--buff = c;
	if (flags->field)
		return (flags->field);
	return (1);
}
