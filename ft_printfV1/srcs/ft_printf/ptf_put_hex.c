/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ptf_put_hex.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/09 11:46:53 by lbrangie          #+#    #+#             */
/*   Updated: 2018/03/09 15:26:19 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static char		*ptf_write_hex(t_flags *flags, char *buff, intmax_t h, int nlen)
{
	char			*str_base;
	int				i;

	str_base = "0123456789abcdef";
	i = 0;
	if (flags->p_flags & PRINTF_F_HASH)
	{
		if (*buff - 1 == ' ')
			buff -= 2;
		*buff++ = '0';
		*buff++ = 'x';
	}
	while (i++ < nlen - 1)
		buff++;
	while (nlen--)
	{
		*buff-- = str_base[ft_abs(h % 16)];
		h /= 16;
	}
	buff += i;
	return (buff);
}

static char		*ptf_write_fill(char *buff, int flen)
{
	while (flen--)
		*buff++ = ' ';
	return (buff);
}

int				ptf_put_hex(t_flags *flags, char *buff, va_list ap)
{
	intmax_t		h;
	int				nlen;
	int				flen;
	int				ret;

	h = ptf_select_ucast(flags, ap);
	nlen = ft_numlen_base(h, 16);
	flen = 0;
	if ((flags->pres >= 0) && (flags->pres > nlen))
		nlen = flags->pres;
	if (flags->field > nlen)
		flen = flags->field - nlen - ptf_ishash(flags);
	ret = flen + nlen;
	if (flags->p_flags & PRINTF_F_MINU)
		buff = ptf_write_hex(flags, buff, h, nlen) + 1;
	buff = ptf_write_fill(buff, flen);
	if (flags->p_flags ^ PRINTF_F_MINU)
		buff = ptf_write_hex(flags, buff, h, nlen);
	return (ret + ptf_ishash(flags));
}
