/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ptf_put_str.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/19 13:56:36 by lbrangie          #+#    #+#             */
/*   Updated: 2018/03/07 18:15:23 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static char		*ptf_write_str(char *buff, char *str, int slen)
{
	while (slen--)
		*buff++ = *str++;
	return (buff);
}

static char		*ptf_write_fill(char *buff, char fill, int flen)
{
	while (flen--)
		*buff++ = fill;
	return (buff);
}

int				ptf_put_str(t_flags *flags, char *buff, va_list ap)
{
	char			*str;
	char			fill;
	int				slen;
	int				flen;
	int				ret;

/*	if (!ft_strcmp(flags->len, "l"))
		unicode;*/
	str = va_arg(ap, char *);
	fill = ' ';
	slen = ft_strlen(str);
	flen = 0;
	if ((flags->p_flags & PRINTF_F_ZERO) && (flags->p_flags ^ PRINTF_F_MINU))
		fill = '0';
	if ((flags->pres >= 0) && (flags->pres < slen))
		slen = flags->pres;
	if (flags->field > slen)
		flen = flags->field - slen;
	ret = flen + slen;
	if (flags->p_flags & PRINTF_F_MINU)
		buff = ptf_write_str(buff, str, slen);
	buff = ptf_write_fill(buff, fill, flen);
	if (flags->p_flags ^ PRINTF_F_MINU)
		buff = ptf_write_str(buff, str, slen);
	return (ret);
}
