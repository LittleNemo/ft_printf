/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_ischarset.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/25 14:36:21 by lbrangie          #+#    #+#             */
/*   Updated: 2018/06/25 14:40:19 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int				ft_str_ischarset(char *str, char *charset)
{
	if (!str || !charset)
		return (0);
	while (*str)
		if (!ft_ischarset(*str++, charset))
			return (0);
	return (1);
}
