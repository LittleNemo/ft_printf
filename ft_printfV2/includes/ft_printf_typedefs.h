/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_typedefs.h                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/16 14:46:07 by lbrangie          #+#    #+#             */
/*   Updated: 2018/03/26 16:04:45 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_TYPEDEFS_H
# define FT_PRINTF_TYPEDEFS_H

/*
**		STRUCT S_FLAGS:
**	The 's_flags' structure contains all the informations about the formatting
**	of the convertions in the string 'format' of ft_printf.
**	 - The attribut flags are stored, using bitwise, in the 'p_flags' char, that
**	can be represented as follow:
**		[][][][ ][+][-][0][#]
**	 - The length modifiers are stored in the string 'length'.
**	 - The field width is stored in 'field'.
**	 - The precision in 'pres'.
**	(Supported flags: '#', '0', '-', '+' and ' ')
**	(Supported length modifier: 'h', 'l', 'j', 'z', 'hh' and 'll')
*/

/*
**		STRUCT S_BUFF:
**	The 's_buff' structure contains multiple informations about the buffer used
**	in the vfprint and vdprint functions.
**	 - The string 'str' is the string that will be printed by the function.
**	 - The integer 'index' is the index where we are at in str.
**	 - The integer 'ret' is the number of bytes wrote by the function.
**	 - The integer 'fd' is the descriptor of the file we have to print into.
*/

/*
**		STRUCT S_COLORS:
**	The 's_colors' structure associates a string of characters, started by a '{'
**	and ended by a '}', in the string 'format' of ft_printf with an escape
**	sequence that will color the text printed.
**	 - The string 'flag' is the string to search into 'format'.
**	 - The string 'print' is the escape sequence to write into t_buff.str to
**	color the text.
*/

/*
**		STRUCT S_CONV:
**	The 's_conv' structure associates a convertion character with the
**	corresponding function in order to compute the requested convertion.
**	 - The character 'flag' is the convertion character..
**	 - The function 'f' is the function associated to the character.
**	(Supported convertions: s, S, p, d, D, i, o, O, u, U, x, X, c, C.)
**	(Bonus convertions: b, B. Like x and X, the int argument is converted to
**	unsigned binary notation.)
*/

typedef struct	s_flags
{
	char			p_flags;
	char			len[4];
	int				field;
	int				pres;
}				t_flags;

typedef struct	s_buff
{
	char			str[PRINTF_BUFF_SIZE];
	int				index;
	int				ret;
	int				fd;
}				t_buff;

typedef struct	s_colors
{
	char			*flag;
	char			*print;
}				t_colors;

typedef struct	s_conv
{
	char			flag;
	void			(*f)(t_flags *, t_buff *, va_list);
}				t_conv;

typedef			unsigned long long
				t_ullong;
#endif
