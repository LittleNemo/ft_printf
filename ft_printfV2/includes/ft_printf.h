/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/08 13:41:14 by lbrangie          #+#    #+#             */
/*   Updated: 2018/03/26 16:53:23 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdarg.h>
# include <stdio.h>
# include "../srcs/libft/includes/libft.h"
# include "ft_printf_defines.h"
# include "ft_printf_typedefs.h"

int				ft_printf(const char *format, ...);
int				ft_fprintf(FILE *stream, const char *format, ...);
int				ft_dprintf(int fd, const char *format, ...);
int				ft_vprintf(const char *format, va_list ap);
int				ft_vfprintf(FILE *stream, const char *format, va_list ap);
int				ft_vdprintf(int fd, const char *format, va_list ap);

const char		*ptf_get_flags(t_flags *flags, const char *format, va_list ap);

int				ptf_isconv(char c);
int				ptf_isflag(char c);

void			ptf_put_str(t_flags *flags, t_buff *buff, va_list ap);
void			ptf_put_wstr(t_flags *flags, t_buff *buff, va_list ap);
void			ptf_put_ptr(t_flags *flags, t_buff *buff, va_list ap);
void			ptf_put_dec(t_flags *flags, t_buff *buff, va_list ap);
void			ptf_put_ldec(t_flags *flags, t_buff *buff, va_list ap);
void			ptf_put_oct(t_flags *flags, t_buff *buff, va_list ap);
void			ptf_put_uoct(t_flags *flags, t_buff *buff, va_list ap);
void			ptf_put_udec(t_flags *flags, t_buff *buff, va_list ap);
void			ptf_put_ludec(t_flags *flags, t_buff *buff, va_list ap);
void			ptf_put_hex(t_flags *flags, t_buff *buff, va_list ap);
void			ptf_put_mhex(t_flags *flags, t_buff *buff, va_list ap);
void			ptf_put_char(t_flags *flags, t_buff *buff, va_list ap);
void			ptf_put_wchar(t_flags *flags, t_buff *buff, va_list ap);
void			ptf_put_bin(t_flags *flags, t_buff *buff, va_list ap);
void			ptf_put_mbin(t_flags *flags, t_buff *buff, va_list ap);

long long		ptf_select_cast(t_flags *flags, va_list ap);
int				ptf_select_color(const char *format, char *buff);
void			ptf_select_conv(t_flags *flags, t_buff *buff, const char *format, va_list ap);
t_ullong		ptf_select_ucast(t_flags *flags, va_list ap);

void			ptf_buff_add_char(t_buff *buff, const char *format);
void			ptf_buff_add_str(t_buff *buff, char *add);
void			ptf_buff_flush(t_buff *buff);
void			ptf_buff_init(t_buff *buff, int fd);

t_colors		*ptf_sgl_colors(void);
t_conv			*ptf_sgl_conv(void);

void			ptf_itoa_base(char *fresh, long long d, int nlen, int base);
void			ptf_uitoa_base(char *fresh, t_ullong d, int nlen, int base);
size_t			ptf_unumlen_base(t_ullong n, int base);
#endif
