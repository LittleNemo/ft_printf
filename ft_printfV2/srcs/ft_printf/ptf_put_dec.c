/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ptf_put_dec.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/12 13:33:14 by lbrangie          #+#    #+#             */
/*   Updated: 2018/03/26 18:33:17 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void		ptf_write_sign(t_flags *flags, t_buff *buff, long long d)
{
	char			sign_pos;

	if ((flags->p_flags & PRINTF_F_SPAC) && (d >= 0))
	{
		sign_pos = ' ';
		ptf_buff_add_char(buff, &sign_pos);
	}
	if ((flags->p_flags & PRINTF_F_PLUS) && (d >= 0))
	{
		sign_pos = '+';
		if (flags->p_flags & PRINTF_F_SPAC)
			buff->index--;
		ptf_buff_add_char(buff, &sign_pos);
	}
}

static void		ptf_write_dec(t_flags *flags, t_buff *buff, long long d, int nlen)
{
	char			fresh[65];

	ptf_write_sign(flags, buff, d);
	ptf_itoa_base(fresh, d, nlen, 10);
	ptf_buff_add_str(buff, fresh);
}

static void		ptf_empty(t_flags *flags, t_buff *buff, int flen, char fill)
{
	if (flags->p_flags & PRINTF_F_MINU)
		ptf_write_sign(flags, buff, 0);
	while (flen-- && flen > 0)
		ptf_buff_add_char(buff, &fill);
	if ((flags->p_flags & PRINTF_F_SPAC) && (flags->p_flags & PRINTF_F_PLUS))
		ptf_buff_add_char(buff, &fill);
	if (!(flags->p_flags & PRINTF_F_MINU))
		ptf_write_sign(flags, buff, 0);
}

void			ptf_put_dec(t_flags *flags, t_buff *buff, va_list ap)
{
	long long		d;
	int				nlen;
	int				flen;
	char			fill;

	d = ptf_select_cast(flags, ap);
	nlen = ft_numlen_base(d, 10);
	flen = 0;
	fill = ' ';
	if ((flags->p_flags & PRINTF_F_ZERO) && !(flags->p_flags & PRINTF_F_MINU))
		fill = '0';
	if ((flags->pres >= 0) && (flags->pres > nlen))
		nlen = flags->pres;
	if (flags->field > nlen)
		flen = flags->field - nlen - (!!((flags->p_flags & PRINTF_F_SPAC) || \
					(flags->p_flags & PRINTF_F_PLUS)) && (d >= 0)) + (d >= 0);
	if (d == 0 && flags->pres == 0)
		return (ptf_empty(flags, buff, flen + 1, fill));
	if (flags->p_flags & PRINTF_F_MINU)
		ptf_write_dec(flags, buff, d, nlen);
	while (flen-- && flen > 0)
		ptf_buff_add_char(buff, &fill);
	if (!(flags->p_flags & PRINTF_F_MINU))
		ptf_write_dec(flags, buff, d, nlen);
}
