/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ptf_put_char.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/14 16:52:49 by lbrangie          #+#    #+#             */
/*   Updated: 2018/03/22 14:40:43 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void			ptf_put_char(t_flags *flags, t_buff *buff, va_list ap)
{
	char			c;
	char			fill;
	int				flen;

/*	if (!ft_strcmp(flags->len, "l"))
		return (ptf_put_wchar(flags, buff, ap));*/
	c = va_arg(ap, int);
	fill = ' ';
	flen = 0;
	if ((flags->p_flags & PRINTF_F_ZERO) && !(flags->p_flags & PRINTF_F_MINU))
		fill = '0';
	if (flags->field)
		flen = flags->field - 1;
	if (flags->p_flags & PRINTF_F_MINU)
		ptf_buff_add_char(buff, &c);
	while (flen--)
		ptf_buff_add_char(buff, &fill);
	if (!(flags->p_flags & PRINTF_F_MINU))
		ptf_buff_add_char(buff, &c);
}
