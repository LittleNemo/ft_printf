/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ptf_utils.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/26 13:56:51 by lbrangie          #+#    #+#             */
/*   Updated: 2018/03/26 16:53:54 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void			ptf_itoa_base(char *fresh, long long d, int nlen, int base)
{
	char			*str_base;
	int				i;
	int				neg;

	str_base = STR_BASE;
	i = 0;
	neg = ft_isneg(d);
	if (base == 10)
		fresh[0] = '-';
	while (i < nlen)
	{
		fresh[nlen - i++ - 1 + neg] = str_base[ft_abs(d % base)];
		d /= base;
	}
	fresh[i + neg] = '\0';
}

void			ptf_uitoa_base(char *fresh, t_ullong d, int nlen, int base)
{
	char			*str_base;
	int				i;

	str_base = STR_BASE;
	i = 0;
	while (i < nlen)
	{
		fresh[nlen - i++ - 1] = str_base[ft_abs(d % base)];
		d /= base;
	}
	fresh[i] = '\0';
}

size_t			ptf_unumlen_base(t_ullong n, int base)
{
	size_t			len;

	len = 0;
	while ((t_ullong)(ft_power(base, len) - 1) < n)
		len++;
	return (len);
}
