/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ptf_put_all.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/15 13:53:55 by lbrangie          #+#    #+#             */
/*   Updated: 2018/03/26 17:11:18 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*void			ptf_put_str(t_flags *flags, t_buff *buff, va_list ap)
{
}*/

void			ptf_put_wstr(t_flags *flags, t_buff *buff, va_list ap)
{
}

void			ptf_put_ptr(t_flags *flags, t_buff *buff, va_list ap)
{
}

/*void			ptf_put_dec(t_flags *flags, t_buff *buff, va_list ap)
{
}*/

void			ptf_put_ldec(t_flags *flags, t_buff *buff, va_list ap)
{
}

void			ptf_put_oct(t_flags *flags, t_buff *buff, va_list ap)
{
}

void			ptf_put_uoct(t_flags *flags, t_buff *buff, va_list ap)
{
}

void			ptf_put_udec(t_flags *flags, t_buff *buff, va_list ap)
{
}

void			ptf_put_ludec(t_flags *flags, t_buff *buff, va_list ap)
{
}

/*void			ptf_put_hex(t_flags *flags, t_buff *buff, va_list ap)
{
}*/

/*void			ptf_put_mhex(t_flags *flags, t_buff *buff, va_list ap)
{
}*/

/*void			ptf_put_char(t_flags *flags, t_buff *buff, va_list ap)
{
}*/

void			ptf_put_wchar(t_flags *flags, t_buff *buff, va_list ap)
{
}

/*void			ptf_put_bin(t_flags *flags, t_buff *buff, va_list ap)
{
}*/

/*void			ptf_put_mbin(t_flags *flags, t_buff *buff, va_list ap)
{
}*/
