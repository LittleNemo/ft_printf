/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ptf_singleton_conv.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/09 16:57:59 by lbrangie          #+#    #+#             */
/*   Updated: 2018/03/15 15:52:25 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void		ptf_init_s_conv_flags(t_conv *tab_conv)
{
	tab_conv[0].flag = 's';
	tab_conv[1].flag = 'S';
	tab_conv[2].flag = 'p';
	tab_conv[3].flag = 'd';
	tab_conv[4].flag = 'D';
	tab_conv[5].flag = 'i';
	tab_conv[6].flag = 'o';
	tab_conv[7].flag = 'O';
	tab_conv[8].flag = 'u';
	tab_conv[9].flag = 'U';
	tab_conv[10].flag = 'x';
	tab_conv[11].flag = 'X';
	tab_conv[12].flag = 'c';
	tab_conv[13].flag = 'C';
	tab_conv[14].flag = 'b';
	tab_conv[15].flag = 'B';
}

static void		ptf_init_s_conv_fonct(t_conv *tab_conv)
{
	tab_conv[0].f = &ptf_put_str;
	tab_conv[1].f = &ptf_put_wstr;
	tab_conv[2].f = &ptf_put_ptr;
	tab_conv[3].f = &ptf_put_dec;
	tab_conv[4].f = &ptf_put_ldec;
	tab_conv[5].f = &ptf_put_dec;
	tab_conv[6].f = &ptf_put_oct;
	tab_conv[7].f = &ptf_put_uoct;
	tab_conv[8].f = &ptf_put_udec;
	tab_conv[9].f = &ptf_put_ludec;
	tab_conv[10].f = &ptf_put_hex;
	tab_conv[11].f = &ptf_put_mhex;
	tab_conv[12].f = &ptf_put_char;
	tab_conv[13].f = &ptf_put_wchar;
	tab_conv[14].f = &ptf_put_bin;
	tab_conv[15].f = &ptf_put_mbin;
}

t_conv			*ptf_sgl_conv(void)
{
	static t_conv	tab_conv[PRINTF_NB_CONV + 1];

	if (!tab_conv[0].flag)
	{
		ptf_init_s_conv_flags(tab_conv);
		ptf_init_s_conv_fonct(tab_conv);
	}
	return (tab_conv);
}
