/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ptf_singleton_colors.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/09 16:58:03 by lbrangie          #+#    #+#             */
/*   Updated: 2018/03/15 15:11:41 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void		ptf_init_s_colors_flags_part1(t_colors *tab_colors)
{
	tab_colors[0].flag = "{eoc}";
	tab_colors[1].flag = "{bold}";
	tab_colors[2].flag = "{dim}";
	tab_colors[3].flag = "{underline}";
	tab_colors[4].flag = "{blink}";
	tab_colors[5].flag = "{red}";
	tab_colors[6].flag = "{green}";
	tab_colors[7].flag = "{blue}";
	tab_colors[8].flag = "{white}";
	tab_colors[9].flag = "{cyan}";
	tab_colors[10].flag = "{magenta}";
	tab_colors[11].flag = "{yellow}";
	tab_colors[12].flag = "{black}";
	tab_colors[13].flag = "{light_blue}";
	tab_colors[14].flag = "{purple}";
	tab_colors[15].flag = "{orange}";
}

static void		ptf_init_s_colors_flags_part2(t_colors *tab_colors)
{
	tab_colors[16].flag = "{Bred}";
	tab_colors[17].flag = "{Bgreen}";
	tab_colors[18].flag = "{Bblue}";
	tab_colors[19].flag = "{Bwhite}";
	tab_colors[20].flag = "{Bcyan}";
	tab_colors[21].flag = "{Bmagenta}";
	tab_colors[22].flag = "{Byellow}";
	tab_colors[23].flag = "{Bblack}";
	tab_colors[24].flag = "{Blight_blue}";
	tab_colors[25].flag = "{Bpurple}";
	tab_colors[26].flag = "{Borange}";
}

static void		ptf_init_s_colors_print_part1(t_colors *tab_colors)
{
	tab_colors[0].print = "\e[0m";
	tab_colors[1].print = "\e[1m";
	tab_colors[2].print = "\e[2m";
	tab_colors[3].print = "\e[4m";
	tab_colors[4].print = "\e[5m";
	tab_colors[5].print = "\e[31m";
	tab_colors[6].print = "\e[32m";
	tab_colors[7].print = "\e[34m";
	tab_colors[8].print = "\e[97m";
	tab_colors[9].print = "\e[36m";
	tab_colors[10].print = "\e[35m";
	tab_colors[11].print = "\e[33m";
	tab_colors[12].print = "\e[30m";
	tab_colors[13].print = "\e[94m";
	tab_colors[14].print = "\e[38;5;55m";
	tab_colors[15].print = "\e[38;5;202m";
}

static void		ptf_init_s_colors_print_part2(t_colors *tab_colors)
{
	tab_colors[16].print = "\e[41m";
	tab_colors[17].print = "\e[42m";
	tab_colors[18].print = "\e[44m";
	tab_colors[19].print = "\e[107m";
	tab_colors[20].print = "\e[46m";
	tab_colors[21].print = "\e[45m";
	tab_colors[22].print = "\e[43m";
	tab_colors[23].print = "\e[40m";
	tab_colors[24].print = "\e[104m";
	tab_colors[25].print = "\e[48;5;55m";
	tab_colors[26].print = "\e[48;5;202m";
}

t_colors		*ptf_sgl_colors(void)
{
	static t_colors	tab_colors[PRINTF_NB_COLORS + 1];

	if (!tab_colors[0].flag)
	{
		ptf_init_s_colors_flags_part1(tab_colors);
		ptf_init_s_colors_flags_part2(tab_colors);
		ptf_init_s_colors_print_part1(tab_colors);
		ptf_init_s_colors_print_part2(tab_colors);
	}
	return (tab_colors);
}
