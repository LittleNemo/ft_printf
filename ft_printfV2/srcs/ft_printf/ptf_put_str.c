/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ptf_put_str.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/19 13:56:36 by lbrangie          #+#    #+#             */
/*   Updated: 2018/03/22 14:41:06 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void		ptf_write_str(t_buff *buff, char *str, int slen)
{
	while (slen--)
		ptf_buff_add_char(buff, str++);
}

void			ptf_put_str(t_flags *flags, t_buff *buff, va_list ap)
{
	char			*str;
	char			fill;
	int				slen;
	int				flen;

/*	if (!ft_strcmp(flags->len, "l"))
		return (ptf_put_wstr(flags, buff, ap));*/
	str = va_arg(ap, char *);
	fill = ' ';
	slen = ft_strlen(str);
	flen = 0;
	if ((flags->p_flags & PRINTF_F_ZERO) && !(flags->p_flags & PRINTF_F_MINU))
		fill = '0';
	if ((flags->pres >= 0) && (flags->pres < slen))
		slen = flags->pres;
	if (flags->field > slen)
		flen = flags->field - slen;
	if (flags->p_flags & PRINTF_F_MINU)
		ptf_write_str(buff, str, slen);
	while (flen--)
		ptf_buff_add_char(buff, &fill);
	if (!(flags->p_flags & PRINTF_F_MINU))
		ptf_write_str(buff, str, slen);
}
