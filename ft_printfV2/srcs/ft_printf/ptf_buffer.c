/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ptf_buffer.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/09 15:46:52 by lbrangie          #+#    #+#             */
/*   Updated: 2018/03/16 16:07:29 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void			ptf_buff_add_char(t_buff *buff, const char *format)
{
	if ((buff->index + 1) > PRINTF_BUFF_SIZE)
	{
		buff->ret += write(buff->fd, buff->str, buff->index);
		ptf_buff_flush(buff);
		buff->index = 0;
	}
	buff->str[buff->index++] = *format;
}

void			ptf_buff_add_str(t_buff *buff, char *add)
{
	unsigned int	i_add;

	i_add = 0;
/*	if ((buff->index + ft_strlen(add)) < PRINTF_BUFF_SIZE)
		while (add[i_add])
			buff->str[buff->index++] = add[i_add++];
	else
	{
		while (buff->index < PRINTF_BUFF_SIZE)
			buff->str[buff->index++] = add[i_add++];
		*ret += write(stream->_file, buff->str, PRINTF_BUFF_SIZE);
		ptf_buff_flush(buff);
		buff->index = 0;
		while (add[i_add])
			buff->str[buff->index++] = add[i_add++];
	}*/
	while (add[i_add])
	{
		if (buff->index < PRINTF_BUFF_SIZE)
			buff->str[buff->index++] = add[i_add++];
		else
		{
			buff->ret += write(buff->fd, buff->str, buff->index);
			ptf_buff_flush(buff);
			buff->index = 0;
		}
	}
}

void			ptf_buff_flush(t_buff *buff)
{
	unsigned int	index;

	index = 0;
	while (index <= PRINTF_BUFF_SIZE)
		buff->str[index++] = '\0';
}

void			ptf_buff_init(t_buff *buff, int fd)
{
	ptf_buff_flush(buff);
	buff->index = 0;
	buff->ret = 0;
	buff->fd = fd;
}
