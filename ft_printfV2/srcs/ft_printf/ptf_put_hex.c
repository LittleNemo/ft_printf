/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ptf_put_hex.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/22 13:38:39 by lbrangie          #+#    #+#             */
/*   Updated: 2018/03/26 18:57:20 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void		ptf_write_pref(t_buff *buff)
{
	char			pref[3];

	pref[0] = '0';
	pref[1] = 'x';
	pref[2] = '\0';
	ptf_buff_add_str(buff, pref);
}

static void		ptf_write_hex(t_flags* flags, t_buff *buff, t_ullong d, int nlen)
{
	char			fresh[65];

	if ((flags->p_flags & PRINTF_F_HASH) && (d != 0))
		ptf_write_pref(buff);
	ptf_uitoa_base(fresh, d, nlen, 16);
	ptf_buff_add_str(buff, ft_str_tolower(fresh));
}

static void		ptf_empty(t_buff *buff, int flen, char fill)
{
	while (flen-- && flen > 0)
		ptf_buff_add_char(buff, &fill);
}

void			ptf_put_hex(t_flags *flags, t_buff *buff, va_list ap)
{
	t_ullong		d;
	int				nlen;
	int				flen;
	char			fill;

	d = ptf_select_ucast(flags, ap);
	nlen = ptf_unumlen_base(d, 16);
	flen = 0;
	fill = ' ';
	if ((flags->p_flags & PRINTF_F_ZERO) && !(flags->p_flags & PRINTF_F_MINU))
		fill = '0';
	if ((flags->pres >= 0) && (flags->pres > nlen))
		nlen = flags->pres;
	if (flags->field > nlen)
		flen = flags->field - nlen;
	if (!!(flags->p_flags & PRINTF_F_HASH) && (d != 0))
		flen -= 2;
	if (d == 0 && flags->pres == 0)
		return (ptf_empty(buff, flen + 2, fill));
	if (flags->p_flags & PRINTF_F_MINU)
		ptf_write_hex(flags, buff, d, nlen);
	while (flen > 0 && flen--)
		ptf_buff_add_char(buff, &fill);
	if (!(flags->p_flags & PRINTF_F_MINU))
		ptf_write_hex(flags, buff, d, nlen);
}
