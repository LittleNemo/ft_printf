/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ptf_select.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/01 15:50:14 by lbrangie          #+#    #+#             */
/*   Updated: 2018/03/26 18:00:56 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

long long		ptf_select_cast(t_flags *flags, va_list ap)
{
	long long		d;

	if (ft_strlen(flags->len) == 3)
		d = va_arg(ap, long long);
	else if ((flags->len[0] == 'h') && (!flags->len[1]))
		d = (short)va_arg(ap, int);
	else if ((flags->len[0] == 'h') && (flags->len[1] == 'h'))
		d = (signed char)va_arg(ap, int);
	else if ((flags->len[0] == 'l') && (!flags->len[1]))
		d = va_arg(ap, long);
	else if ((flags->len[0] == 'l') && (flags->len[1] == 'l'))
		d = va_arg(ap, long long);
	else if ((flags->len[0] == 'j') && (!flags->len[1]))
		d = va_arg(ap, intmax_t);
	else if ((flags->len[0] == 'z') && (!flags->len[1]))
		d = va_arg(ap, ssize_t);
	else
		d = va_arg(ap, int);
	return (d);
}

t_ullong		ptf_select_ucast(t_flags *flags, va_list ap)
{
	t_ullong		d;

	if (ft_strlen(flags->len) == 3)
		d = va_arg(ap, unsigned long long);
	else if ((flags->len[0] == 'h') && (!flags->len[1]))
		d = (unsigned short)va_arg(ap, unsigned int);
	else if ((flags->len[0] == 'h') && (flags->len[1] == 'h'))
		d = (unsigned char)va_arg(ap, unsigned int);
	else if ((flags->len[0] == 'l') && (!flags->len[1]))
		d = va_arg(ap, unsigned long);
	else if ((flags->len[0] == 'l') && (flags->len[1] == 'l'))
		d = va_arg(ap, unsigned long long);
	else if ((flags->len[0] == 'j') && (!flags->len[1]))
		d = va_arg(ap, unsigned  int);
	else if ((flags->len[0] == 'z') && (!flags->len[1]))
		d = va_arg(ap, size_t);
	else
		d = va_arg(ap, unsigned  int);
	return (d);
}

int				ptf_select_color(const char *format, char *buff)
{
	unsigned int	i;
	unsigned int	j;
	t_colors		*tab_colors;

	i = 0;
	tab_colors = ptf_sgl_colors();
	while (i < PRINTF_NB_COLORS)
	{
		if (!ft_strncmp(format, tab_colors[i].flag, ft_strlen(tab_colors[i].flag)))
		{
			j = 0;
			while (tab_colors[i].print[j])
				*buff++ = tab_colors[i].print[j++];
			return (ft_strlen(tab_colors[i].print));
		}
		i++;
	}
	return (0);
}

void			ptf_select_conv(t_flags *flags, t_buff *buff, const char *format, va_list ap)
{
	unsigned int	i;
	t_conv			*tab_conv;

	i = 0;
	tab_conv = ptf_sgl_conv();
	while (i < PRINTF_NB_CONV)
		if (*format == tab_conv[i++].flag)
			return (tab_conv[--i].f(flags, buff, ap));
}
