/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vprintf.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/09 15:37:59 by lbrangie          #+#    #+#             */
/*   Updated: 2018/03/26 13:29:58 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int				ft_vprint(const char *format, va_list ap)
{
	return (ft_vfprintf(stdout, format, ap));
}

int				ft_vfprintf(FILE *stream, const char *format, va_list ap)
{
	t_buff			buff;
	t_flags			flags;

	ptf_buff_init(&buff, stream->_file);
	while (*format)
	{
		if (*format == '%')
		{
			format = ptf_get_flags(&flags, ++format, ap);
			if (ptf_isconv(*format))
				ptf_select_conv(&flags, &buff, format++, ap);
		}
//		if (*format == '{')
		else
			ptf_buff_add_char(&buff, format++);
	}
	return (buff.ret += write(buff.fd, buff.str, buff.index));
}
