/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/10 13:06:22 by lbrangie          #+#    #+#             */
/*   Updated: 2018/06/13 17:10:48 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <limits.h>
#include <locale.h>
#include <wchar.h>
#include "ft_printf.h"

int				main(void)
{
	setlocale(LC_ALL, "");
	wchar_t		test_pick = L'♤';
	wchar_t		*test_cards = L"♤ ♡ ♢ ♧";
	wchar_t		*test_null = NULL;
	wchar_t		test_uni = L'🦄';
	wchar_t		*test_unis = L"🦄🦄🦄🦄";
	wchar_t		*test_china = L"我是一只猫。";
	char		*test = "lol";
	int			field = 10;
	int			pres = -1;
	int			ret_you;
	int			ret_sys;

	ret_you = ft_printf("==>YOU: '%s'\n", test);
	ret_sys =    printf("==>SYS: '%s'\n", test);
	printf("ret YOU: %d\n", ret_you);
	printf("ret SYS: %d\n", ret_sys);
	printf("==================== COLORS ====================\n");
	ft_printf("Default %@ Red %@ Default\n", RED, EOC);
	ft_printf("Default %@ Green %@ Default\n", GREEN, EOC);
	ft_printf("Default %@ Blue %@ Default\n", BLUE, EOC);
	ft_printf("Default %@ White %@ Default\n", WHITE, EOC);
	ft_printf("Default %@ Cyan %@ Default\n", CYAN, EOC);
	ft_printf("Default %@ Magenta %@ Default\n", MAGENTA, EOC);
	ft_printf("Default %@ Yellow %@ Default\n", YELLOW, EOC);
	ft_printf("Default %@ Black %@ Default\n", BLACK, EOC);
	ft_printf("Default %@ Light Blue %@ Default\n", LIGHT_BLUE, EOC);
	ft_printf("Default %@ Purple %@ Default\n", PURPLE, EOC);
	ft_printf("Default %@ Orange %@ Default\n", ORANGE, EOC);
	printf("=============== BACKGROUND COLORS ==============\n");
	ft_printf("Default %@ Background Red %@ Default\n", BRED, EOC);
	ft_printf("Default %@ Background Green %@ Default\n", BGREEN, EOC);
	ft_printf("Default %@ Background Blue %@ Default\n", BBLUE, EOC);
	ft_printf("Default %@ Background White %@ Default\n", BWHITE, EOC);
	ft_printf("Default %@ Background Cyan %@ Default\n", BCYAN, EOC);
	ft_printf("Default %@ Background Magenta %@ Default\n", BMAGENTA, EOC);
	ft_printf("Default %@ Background Yellow %@ Default\n", BYELLOW, EOC);
	ft_printf("Default %@ Background Black %@ Default\n", BBLACK, EOC);
	ft_printf("Default %@ Background Light Blue %@ Default\n", BLIGHT_BLUE, EOC);
	ft_printf("Default %@ Background Purple %@ Default\n", BPURPLE, EOC);
	ft_printf("Default %@ Background Orange %@ Default\n", BORANGE, EOC);
	printf("==================== FORMATS ===================\n");
	ft_printf("Default %@ Bold %@ Default\n", BOLD, EOC);
	ft_printf("Default %@ Dim %@ Default\n", DIM, EOC);
	ft_printf("Default %@ Underline %@ Default\n", UNDERLINE, EOC);
	ft_printf("Default %@ Blink %@ Default\n", BLINK, EOC);
	ft_printf("Default %@ Hidden %@ Default\n", HIDDEN, EOC);
	ft_printf("Default %@ Inverted %@ Default\n", INVERTED, EOC);
	ft_printf("Default %@ Italic %@ Default\n", ITALIC, EOC);
	return (0);
}
